from rest_framework import status
from ..http_client import BasicAuthHTTPClient
from ..objects import ProcessingResult
import json
import logging


logger = logging.getLogger(__name__)


class ReportAPIClient(BasicAuthHTTPClient):

    def __init__(self, host: str, ssl: bool = False, username: str = None, password: str = None):
        super().__init__(host, ssl, username, password)
        self.header_user_agent = f'v4_ReportAPI_HTTPClient'
        self.header_content_type = 'application/json;charset=utf-8'
        self.base_url = 'mp/reports'

    def _simple_report(self, entry: str = None, **kwargs):
        kwargs['o'] = 'json'
        response = self.get(self.url_with_params(entry, **kwargs))
        result = ProcessingResult()
        result.status = response.status_code
        if response.status_code == status.HTTP_200_OK:
            try:
                result.data = json.loads(response.content)
                result.error = False
            except:
                logger.error(f'Get response data error for: {response}')
                result.data = {'detail': 'Get response data error!'}
                result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        else:
            try:
                result.data = {'detail': response.data['detail']}
            except:
                result.data = {'detail': self.html_as_text(response.text)}
        return result

    def simple_report(self, entry: str = None, **kwargs):
        result = self._simple_report(entry, **kwargs)
        if not result.error:
            if 'data' in result.data:
                result.data = result.data['data']
            else:
                result.error = True
                result.data = {'detail': f'Unexpected data structure error {result.data}'}
                result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        return result
