from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from django.http import HttpResponse
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework_nested import routers
from entry.views_debug import TestViewSet


schema_view = get_schema_view(
    openapi.Info(
        title="UPK2 Connector API",
        default_version='v1.0',
        description="Welcome to the UPK2 Connector!",
        terms_of_service="https://upk2-connector.ctm",
        contact=openapi.Contact(email="admin@upk2-connector.ctm"),
        license=openapi.License(name="Internal"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
    # url='http://upk2-connector.ctm'
)


def index(request):
    return HttpResponse(content="It's OK!", content_type='text', status=200)


debug_router = routers.SimpleRouter()
debug_router.register(r'test', TestViewSet, basename='test')


urlpatterns = [
    path('', index, name='index'),
    path('debug/', include(debug_router.urls)),
    re_path(r'^doc(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('doc/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    # path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    path('admin/', admin.site.urls),
    re_path(r'^api-auth/', include('rest_framework.urls')),
    path('api/private/v1.0/', include('entry.urls')),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)