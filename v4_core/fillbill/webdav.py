from lxml import etree, objectify
from typing import Union, Optional, List, Any
import logging
from collections import defaultdict
from .objects import *


logger = logging.getLogger(__name__)


class WebDAV:
    @staticmethod
    def additional_namespaces() -> dict:
        return {
            'D': 'urn:fill-bill.ru:WebDav-Doc-Props:1.0.0',
            'P': 'urn:fill-bill.ru:WebDav-Pkg-Props:1.0.0',
            'C': 'urn:fill-bill.ru:WebDav-Pkg-CustomProps:1.0.0',
            'X': 'urn:fill-bill.ru:WebDav-Pkg-ExtProps:1.0.0',
            'F': 'urn:fill-bill.ru:WebDav-File-Props:1.0.0',
        }

    @staticmethod
    def search_request_xml(select=None, depth=None, where=None):
        return f'''
                    <searchrequest xmlns="DAV:"
                                   xmlns:D="urn:fill-bill.ru:WebDav-Doc-Props:1.0.0"
                                   xmlns:P="urn:fill-bill.ru:WebDav-Pkg-Props:1.0.0"
                                   xmlns:C="urn:fill-bill.ru:WebDav-Pkg-CustomProps:1.0.0"
                                   xmlns:X="urn:fill-bill.ru:WebDav-Pkg-ExtProps:1.0.0"
                                   xmlns:F="urn:fill-bill.ru:WebDav-File-Props:1.0.0">
                        <basicsearch>
                            <select>{select}</select>
                            <from>
                                <scope>
                                      <href>/mp/packages/</href>
                                      <depth>{depth}</depth>
                                </scope>
                            </from>
                            <where>
                                {
                                where
                                }
                            </where>
                        </basicsearch>
                    </searchrequest>'''

    @staticmethod
    def parse_response(response_raw: str) -> List[dict]:
        try:
            root = objectify.fromstring(response_raw)
        except etree.XMLSyntaxError:
            logger.error(f'XMLSyntaxError loading >> {response_raw}')
            return []

        if not hasattr(root, 'response'):
            logger.error(f'No "response" nodes in >> {response_raw}')
            return []

        items = []
        namespaces = {uri: name for name, uri in WebDAV.additional_namespaces().items()}
        namespaces['DAV:'] = 'M'
        for response in root.response:
            if response.propstat.status == 'HTTP/1.1 200 OK':
                data = defaultdict(dict)
                data['href'] = response.href.text
                for item_property in response.propstat.prop.getchildren():
                    tag = etree.QName(item_property)
                    if tag.localname == 'key':
                        if 'keys' not in data[namespaces[tag.namespace]]:
                            data[namespaces[tag.namespace]]['keys'] = []
                        key = item_property.attrib
                        key['text'] = item_property.text
                        data[namespaces[tag.namespace]]['keys'].append(key)
                    else:
                        data[namespaces[tag.namespace]][tag.localname] = item_property.text
                items.append(data)
        return items

    @staticmethod
    def get_attribute(data: dict, group: str, name: str, default: Any = ''):
        if group not in data:
            logger.warning(f'Namespace alias "{group}" not in {data}')
            return default

        if name not in data[group]:
            logger.warning(f'Name "{name}" with namespace alias "{group}" not in {data}')
            return default
        return data[group][name]

    @staticmethod
    def packages_from_response(response_raw: str) -> Dict[int, Package]:
        items = WebDAV.parse_response(response_raw)
        packages = {}
        for item in items:
            if 'F' in item:
                file = PackageFile(
                    id=WebDAV.get_attribute(item, 'F', 'id'),
                    package_id=WebDAV.get_attribute(item, 'P', 'pid'),
                    md5=WebDAV.get_attribute(item, 'F', 'md5'),
                    name=WebDAV.get_attribute(item, 'M', 'displayname'),
                    filename=WebDAV.get_attribute(item, 'D', 'type'),
                    modification_datetime=WebDAV.get_attribute(item, 'M', 'getlastmodified'),
                    length=WebDAV.get_attribute(item, 'M', 'getcontentlength'),
                    content_type=WebDAV.get_attribute(item, 'M', 'getcontenttype'),
                )
                packages[file.package_id].files.append(file)
            elif len(item['D']):
                document = PackageDocument(
                    id=WebDAV.get_attribute(item, 'D', 'did'),
                    package_id=WebDAV.get_attribute(item, 'P', 'pid'),
                    name=WebDAV.get_attribute(item, 'M', 'displayname'),
                    filename=WebDAV.get_attribute(item, 'D', 'type'),
                    modification_datetime=WebDAV.get_attribute(item, 'M', 'getlastmodified'),
                    length=WebDAV.get_attribute(item, 'M', 'getcontentlength'),
                    # keys=WebDAV.get_keys(item, 'D', 'key'),
                )
                packages[document.package_id].documents.append(document)
            else:
                package = Package(
                    id=WebDAV.get_attribute(item, 'P', 'pid'),
                    creation_datetime=WebDAV.get_attribute(item, 'M', 'creationdate'),
                    modification_datetime=WebDAV.get_attribute(item, 'M', 'getlastmodified'),
                    receiver=WebDAV.get_attribute(item, 'X', 'receiver', 0),
                    sender=WebDAV.get_attribute(item, 'X', 'sender', 0),
                    owner=WebDAV.get_attribute(item, 'P', 'owner', 0),
                    uuid=WebDAV.get_attribute(item, 'X', 'uuid', 0),
                    name=WebDAV.get_attribute(item, 'X', 'name'),
                    status=WebDAV.get_attribute(item, 'P', 'status', 0),
                    parent=WebDAV.get_attribute(item, 'P', 'chainid', 0),
                    # properties=WebDAV.get_properties(item, 'C'),
                )
                package.name = ''
                packages[package.id] = package

        return packages
