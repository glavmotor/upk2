import attr
from datetime import datetime
from dateutil import parser
from typing import Optional, Union, Dict, List


def converter_datetime(source: Union[Optional[str], Optional[datetime]]) -> Optional[datetime]:
    if isinstance(source, datetime):
        return source

    if source is None or source == '':
        return None

    return parser.parse(source)


def converter_clean_str(source: str = '') -> str:
    if source is None:
        return ''

    return source.strip()


@attr.s
class BaseMessage(object):
    id: int = attr.ib(converter=int)
    processed: bool = attr.ib(converter=bool)
    createdate: datetime = attr.ib(converter=converter_datetime)
    processeddate: datetime = attr.ib(converter=converter_datetime)
    preparationdatetime: datetime = attr.ib(converter=converter_datetime)
    isincoming: bool = attr.ib(converter=bool)
    envelopeid: str = attr.ib()
    messagecode: str = attr.ib()
    profileid: str = attr.ib()
    retrycount: int = attr.ib(converter=int)
    documentid: str = attr.ib()
    refdocumentid: str = attr.ib()
    procid: int = attr.ib(converter=int)


@attr.s
class Message(BaseMessage):
    recovery: bool = attr.ib(default=None, converter=bool)
    usersign: bool = attr.ib(default=None, converter=bool)
    initialenvelopeid: str = attr.ib(default='')
    state: str = attr.ib(default='')
    processid: str = attr.ib(default='')
    package_id: int = attr.ib(default=0, converter=int)
    chain_id: int = attr.ib(default=0, converter=int)
    tin: str = attr.ib(default='')
    exchangetype: int = attr.ib(default=0, converter=int)
    customscode: int = attr.ib(default=0, converter=int)
    uid: int = attr.ib(default=0, converter=int)
    albumversion: str = attr.ib(default='')
    usersignid: str = attr.ib(default='')
    resendcount: int = attr.ib(default=0, converter=int)
    initial_message: BaseMessage = attr.ib(default=None)
    from .fillbill import FillBill
    fillbill: FillBill = attr.ib(default=None)

    def body(self):
        if self.fillbill is None:
            return None
        return self.fillbill.messages.get_body(self.id)

    def initial_body(self):
        if self.fillbill is None or self.initial_message is None:
            return None
        return self.fillbill.messages.get_body(self.initial_message.id)



@attr.s
class PackageFile(object):
    id: int = attr.ib(converter=int)
    package_id: int = attr.ib(converter=int)

    md5: str = attr.ib()
    name: str = attr.ib()
    filename: str = attr.ib()
    modification_datetime: datetime = attr.ib(converter=converter_datetime)
    length: int = attr.ib(converter=int, default=0)
    content_type: str = attr.ib(default='')


@attr.s
class PackageDocument(object):
    id: int = attr.ib(converter=int)
    package_id: int = attr.ib(converter=int)

    name: str = attr.ib()
    filename: str = attr.ib()
    modification_datetime: datetime = attr.ib(converter=converter_datetime)
    length: int = attr.ib(converter=int, default=0)
    keys: Dict[str, List[str]] = attr.ib(default=attr.Factory(dict))

#
# @attr.s
# class PackageDocuments(object):
#     documents: List[PackageDocument] = attr.ib(default=attr.Factory(list))
#
#     def append(self, document: PackageDocument):
#         self.documents.append(document)
#
#     def list(self):
#         return self.documents


@attr.s
class Package(object):
    from .fillbill import FillBill
    fillbill: FillBill = attr.ib(default=None)
    id: int = attr.ib(converter=int, default=0)
    creation_datetime: datetime = attr.ib(converter=converter_datetime, default=None)
    modification_datetime: datetime = attr.ib(converter=converter_datetime, default=None)

    receiver: int = attr.ib(default=0, converter=int)
    sender: int = attr.ib(default=0, converter=int)
    owner: int = attr.ib(default=0, converter=int)
    uuid: str = attr.ib(default='')
    name: str = attr.ib(default='')
    status: int = attr.ib(default=0, converter=int)
    parent: int = attr.ib(default=0, converter=int)
    properties: dict = attr.ib(default=attr.Factory(dict))

    files: List[PackageFile] = attr.ib(default=attr.Factory(list))
    documents: List[PackageDocument] = attr.ib(default=attr.Factory(list))
    messages: List[Message] = attr.ib(default=attr.Factory(list))

    def commit(self):
        if self.fillbill is None:
            return False
        return self.fillbill.packages._commit_package(package_id=self.id)

    def new_from_this(self, copy_documents: bool = False, receiver: int = None):
        if self.fillbill is None:
            return None
        return self.fillbill.packages.new_from_package(from_package_id=self.id, copy_documents=copy_documents, receiver=receiver)

    def new_document(self, doctype: str, data=None):
        if self.fillbill is None:
            return None
        return self.fillbill.packages.new_document_in_package(package_id=self.id, doctype=doctype, data=data)

    def new_file(self, filename: str, filepath: str):
        if self.fillbill is None:
            return None
        return self.fillbill.packages.new_file_in_package(package_id=self.id, filename=filename, filepath=filepath)

    def copy_documents_from(self, from_package_id: int):
        if self.fillbill is None:
            return False
        return self.fillbill.packages.copy_docs_from_to(from_package_id=from_package_id, to_package_id=self.id)


