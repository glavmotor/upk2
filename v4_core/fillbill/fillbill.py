from ..http_client import BasicAuthHTTPClient
import platform


class FillBill:

    def __init__(self, host: str, username: str = None, password: str = None, super_username: str = None, super_password: str = None, ssl: bool = False):
        if username is not None:
            username = username.encode('utf-8')
        if password is not None:
            password = password.encode('utf-8')
        # if super_username is not None:
        #     super_username = username.encode('utf-8')
        # if super_password is not None:
        #     super_password = password.encode('utf-8')
        self._username = username
        self._password = password
        self._super_username = super_username
        self._super_password = super_password
        self._client = BasicAuthHTTPClient(host, ssl, username, password)
        self._client.base_url = 'mp'
        self._client.header_user_agent = f'TDClient/10000 ({platform.platform()} , Python {platform.python_version()}, Portal-API 1.0)'
        from .packages import Packages
        self._packages = Packages(self)
        from .messages import Messages
        self._messages = Messages(self)
        from .report_api import ReportAPI
        self._report_api = ReportAPI(self)

    @property
    def requests_from_superuser(self):
        return self.client.username == self._username

    @requests_from_superuser.setter
    def requests_from_superuser(self, value: bool):
        if value:
            self.client.username = self._super_username
            self.client.password = self._super_password
        else:
            self.client.username = self._username
            self.client.password = self._password

    @property
    def client(self):
        return self._client

    @property
    def packages(self):
        return self._packages

    @property
    def messages(self):
        return self._messages

    @property
    def report_api(self):
        return self._report_api
