from ..http_client import HTTPClient
from ..objects import ProcessingResult
from rest_framework import status
# import logging
import json


try:
    from celery.utils.log import get_task_logger
    logger = get_task_logger(__name__)
except:
    import logging
    logger = logging.getLogger(__name__)


class TkClient(HTTPClient):
    def __init__(self, host: str, ssl: bool = False, cookies: str =None, username: str = None, password: str = None):
        super().__init__(host, ssl)
        self._username = username
        self._password = password
        self._cookies = cookies
        self.header_content_type = 'application/json'
        self.header_user_agent = f'OTMConnectorClient'
        self.header_content_type = 'application/json;charset=utf-8'
        # self._base_url = 'otmgate/api'  # Test url
        self._base_url = 'api'
        self.timeout = 1000

    def headers(self) -> {}:
        headers = super().headers()
        if self._cookies is not None:
            headers['Cookie'] = self._cookies
        return headers

    def update_cookies(self) -> (bool, str):
        response = self.post('Auth/Login', data=json.dumps({'Login': self._username, 'Password': self._password}), verify=False)
        if response.status_code != status.HTTP_200_OK:
            res_text = None
            try:
                res_text = response.text
            except:
                res_text = 'n/a'
            logger.error(f'Login request to {self.host} return {response.status_code} with {res_text}')
            return False, None
        if response.content != b'true':
            self._cookies = None
            logger.error(f'Login request to {self.host} return {response.status_code} with {response.text}')
            return False, None
        self._cookies = response.headers.get('Set-Cookie')
        return True, self._cookies

    @staticmethod
    def process_result(response):
        result = ProcessingResult()
        result.status = response.status_code
        if response.status_code in (status.HTTP_200_OK, status.HTTP_201_CREATED, status.HTTP_204_NO_CONTENT):
            try:
                result.data = json.loads(response.content)
                result.error = False
            except:
                pass
            if result.error:
                try:
                    result.data = response.data
                    result.error = False
                except:
                    logger.error(f'Get response data error: {response}')
                    result.data = {'detail': 'Get response data error!'}
                    result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        else:
            try:
                result.data = {'detail': response.data['detail']}
            except:
                result.data = {'detail': HTTPClient.html_as_text(response.text)}
        return result

    def send_api(self, method: str, data=None):
        response = self.post(entry='/'.join(('otmgate', method)), json=data, verify=False) #
        return TkClient.process_result(response)
