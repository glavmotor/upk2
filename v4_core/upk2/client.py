from ..http_client import HTTPClient
from ..objects import ProcessingResult
from rest_framework import status
from requests import request, session
from html2text import html2text
from datetime import datetime


class UPK2Client(HTTPClient):
    def __init__(self, host: str, ssl: bool = False, key: str = None):
        super().__init__(host, ssl)
        self._key = key
        self._header_authorization = key
        self.header_user_agent = f'CTM-UPK2-Connector'
        self.header_content_type = 'application/json;charset=utf-8'
        self._base_url = 'api'
        self.timeout = 1000

    def _json_request(self, method: str = None, entry: str = None, data=None):
        result = ProcessingResult()
        try:
            response = request(method=method,
                               url=f'{self.base_url}/{entry}',
                               headers=self.headers(),
                               timeout=self.timeout,
                               json=data,
                               )
            if response.status_code in (status.HTTP_200_OK, status.HTTP_201_CREATED):
                result.error = False
                result.data = response.json()
            else:
                result.detail = html2text(response.text)
            result.status = response.status_code
        except Exception as e:
            result.error = True
            result.detail = e.__str__()
            result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        return result

    def packages_list(self, datetime_from: datetime = None):
        return self._json_request(method='post',
                                  entry='otm/GetDocumentsInReview',
                                  data={'StartDate': datetime_from.strftime('%Y.%m.%d %H:%M:%S')}
                                  )

    def package_file_list(self, shipment_gid: str):
        return self._json_request(method='post',
                                  entry='otm/GetShipmentDocuments',
                                  data={'ShipmentGID': shipment_gid}
                                  )

    def file_body(self, document_content_gid: str):
        return self._json_request(method='post',
                                  entry='otm/GetDocumentContent',
                                  data={'DocumentContentGID': document_content_gid}
                                  )

    def send_package_status(self, shipment_gid: str, upk2_status: str, comment: str = None):
        return self._json_request(method='post',
                                  entry='otm/SetDocumentStatus',
                                  data={'ShipmentGID': shipment_gid,
                                        'State': upk2_status,
                                        'Comments': comment}
                                  )

    def send_package_document(self, shipment_gid: str, mime_type: str, def_gid: str = None, content_text: str = None, content_binary: str = None):
        return self._json_request(method='post',
                                  entry='otm/CreateDocument',
                                  data={'ShipmentGID': shipment_gid,
                                        # 'EquipmentNumber ': None,
                                        # 'RailStation': None,
                                        # 'OrderNumber': None,
                                        'DocumentDefGID': def_gid,
                                        'DocMimeType': mime_type,
                                        'DocContentText': content_text,
                                        'DocContentBinary': content_binary,
                                        # 'DocNumber': None,
                                        # 'DocDate': None,
                                        }
                                  )
