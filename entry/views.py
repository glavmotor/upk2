from rest_framework import permissions, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.http import HttpResponseRedirect


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated,))
def logout(request):
    ''' Simply delete the token to force a logout'''
    request.user.auth_token.delete()
    request.session.flush()
    return HttpResponseRedirect('/')