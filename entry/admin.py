from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _
from .models import *


########################################################################################################################
#                                                Entry models                                                        #
########################################################################################################################
@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = ('section', 'name', 'value')
    ordering = ('section', 'name',)
    list_filter = ('section',)


@admin.register(User)
class UserAdmin(DjangoUserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password', 'company_id')}),
        (_('Personal info'), {'fields': ('first_name', 'middle_name', 'last_name', 'phone')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'company_id', 'is_active', 'is_staff', 'is_superuser', 'last_login', 'create_date', 'modify_date')
    search_fields = ('email',)
    ordering = ('email',)
    # list_filter = ('company_id',)


########################################################################################################################
#                                             Fill-Bill models                                                         #
########################################################################################################################
@admin.register(FBHost)
class FBHostAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'enabled', 'alias', 'ssl', 'connector_alias', 'superuser')
    ordering = ('name',)


@admin.register(CustomsStage)
class CustomsStageAdmin(admin.ModelAdmin):
    list_display = ('alias', 'name')


@admin.register(CustomsState)
class CustomsStateAdmin(admin.ModelAdmin):
    list_display = ('name', 'process_order', 'alias','stage', 'upk2_send', 'upk2_status', 'upk2_with_regnum', 'upk2_send_pair_bodies')
    list_filter = ('stage',)


@admin.register(MessagesPair)
class MessagesPairAdmin(admin.ModelAdmin):
    list_display = ('name', 'state', 'enabled', 'alias', 'initial_code', 'initial_comment', 'code', 'comment')
    ordering = ('state', 'name', 'code')
    list_filter = ('state', 'code')


@admin.register(ContractorState)
class ContractorStateAdmin(admin.ModelAdmin):
    list_display = ('name', 'process_order', 'alias', 'upk2_send', 'upk2_status')


@admin.register(ContractorStatusTransition)
class ContractorStatusTransitionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'alias', 'state', )
    list_filter = ('state',)
