from .fillbill import FillBill
import logging
from rest_framework import status
from bs4 import BeautifulSoup
from .webdav import WebDAV
# import weakref


logger = logging.getLogger(__name__)


class Packages:

    def __init__(self, fillbill: FillBill):
        self._fillbill = fillbill
        self._packages_url = 'packages'
        self._next_chain_package_url = 'cl/newpkg'

    @property
    def fillbill(self):
        return self._fillbill

    def _search_request(self, select=None, depth=None, where=None):
        self.fillbill.client.header_content_type = 'text/xml'
        response = self.fillbill.client.search(self._packages_url, data=WebDAV.search_request_xml(select, depth, where))
        if response.status_code != status.HTTP_207_MULTI_STATUS:
            logtext = 'Unknown error!'

            try:
                logtext = response.data['detail']
            except:
                pass
            try:
                logtext = self.fillbill.client.html_as_text(response.text)
            except:
                pass

            logger.error(logtext)
            return None
        packages = WebDAV.packages_from_response(response.content)
        if len(packages) > 0:
            for package in packages:
                packages[package].fillbill = self._fillbill
        return packages

    def raw_search(self, select=None, depth=None, where=None):
        return self._search_request(select, depth, where)

    def get(self, pid=None, children=False, messages=False):
        if pid is None:
            return None
        if children:
            depth = 'infinity'
        else:
            depth = '0'
        select = '<allprop />'
        where = f'<eq><prop><P:pid/></prop><literal>{pid}</literal></eq>'
        packages = self._search_request(select, depth, where)
        if packages is None or len(packages) != 1:
            return None
        package = packages[pid]
        if messages:
            package.messages = self.fillbill.messages.search(pid=pid)
        return package

    def _create_package(self, from_package_id: int = None, copy_documents: bool = False, receiver: int = None):
        if from_package_id is not None and copy_documents:
            files = {'parent': (None, from_package_id), 'new-copy-chain': (None, 'Редактировать')}
            response = self.fillbill.client.post(entry=self._next_chain_package_url, files=files, allow_redirects=False)
            if response.status_code == status.HTTP_302_FOUND:
                html = BeautifulSoup(response.content, 'lxml')
                try:
                    pid = int(html.find('a').get('href').lstrip(f'{self.fillbill.client.base_url}/cl/pkgfiles-'))
                    return pid
                except ValueError:
                    logger.error(f'Server response processing error in: {response.content}')
                    return None
            else:
                logger.error(self.fillbill.client.html_as_text(response.content))
                return None
        else:
            data = [f'dst={receiver}']
            if from_package_id is not None:
                data.append(f'pname={from_package_id}')
            response = self.fillbill.client.post(entry=self._packages_url, data='&'.join(data).encode('utf-8'))
            if response.status_code != status.HTTP_200_OK:
                logger.error(self.fillbill.client.html_as_text(response.content))
                return None
            return int(response.text)

    def _package_action(self, package_id: int, **kwargs):
        return self.fillbill.client.post(entry=f'{self._packages_url}/{package_id}', params=kwargs)

    def _commit_package(self, package_id: int):
        response = self._package_action(package_id, action='commit')
        return response.status_code in (status.HTTP_200_OK, status.HTTP_202_ACCEPTED)

    def new(self, receiver: int = None):
        pid = self._create_package(receiver=receiver)
        if pid is None:
            return None
        return self.get(pid=pid)

    def new_from_package(self, from_package_id: int, copy_documents: bool = False, receiver: int = None):
        pid = self._create_package(from_package_id=from_package_id, copy_documents=copy_documents, receiver=receiver)
        if pid is None:
            return None
        return self.get(pid=pid)

    def copy_docs_from_to(self, from_package_id: int, to_package_id: int) -> bool:
        from_package = self.get(pid=from_package_id, children=True)
        if from_package is None:
            logger.error(f'Get from_package (pid={from_package_id}) error')
            return False
        to_package = self.get(pid=to_package_id, children=True)
        if to_package is None:
            logger.error(f'Get to_package (pid={to_package_id}) error')
            return False
        error_count = 0
        for document in from_package.documents:
            body = self.get_document_body(package_id=from_package_id, document_id=document.id)
            did = self.new_document_in_package(package_id=to_package_id, doctype=document.filename, data=body)
            if did is None:
                error_count += 1
        return error_count == 0

    def new_document_in_package(self, package_id: int, doctype: str, data=None):
        response = self.fillbill.client.put(entry=f'{self._packages_url}/{package_id}/documents/{doctype}', data=data)
        if response.status_code in (status.HTTP_200_OK, status.HTTP_202_ACCEPTED):
            try:
                did = int(response.text)
                return did
            except ValueError:
                logger.error(f'Convert error: value {response.text} to int')
                return None
        logger.error(f'Creating document {doctype} in package {package_id} get error: {self.fillbill.client.html_as_text(response.content)}')
        return None

    def get_document_body(self, package_id: int, document_id: int):
        response = self.fillbill.client.get(f'packages/{package_id}/documents/{document_id}')
        if response.status_code != status.HTTP_200_OK:
            logger.error(self.fillbill.client.html_as_text(response.text))
            return None
        return response.content

    def new_file_in_package(self, package_id: int, filename=None, filepath=None, data=None):
        entry = f'{self._packages_url}/{package_id}/files/{filename}'
        if filepath is not None:
            try:
                with open(filepath, 'rb') as f:
                    response = self.fillbill.client.put(entry=entry,
                                                        headers={'Content-type': 'application/octet-stream'},
                                                        data=f)
                if response.status_code == status.HTTP_200_OK:
                    return int(response.content)
                logger.error(self.fillbill.client.html_as_text(response.text))
                return None
            except Exception as e:
                logger.error(f'File uploading error: {e}')
                return None
        logger.error(f'File path is None')
        return None

