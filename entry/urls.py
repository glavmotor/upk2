from rest_framework.authtoken.views import obtain_auth_token
from .views import logout
from django.urls import path, include


urlpatterns = [
    path('auth/', obtain_auth_token, name='api_token_auth'),
    path('logout/', logout, name='api_token_logout'),
]
