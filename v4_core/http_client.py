import html2text
from requests import request
from rest_framework import status
from rest_framework.response import Response
import logging
import json
from .objects import ProcessingResult
from requests.exceptions import HTTPError


try:
    from celery.utils.log import get_task_logger
    logger = get_task_logger(__name__)
except:
    logger = logging.getLogger(__name__)


def if_none(val=None, defval=None):
    if val is None:
        return defval
    return val


class HTTPClient:

    # def __init__(self, host: str, key: str = None, username: str = None, password: str = None, ssl: bool = False):
    def __init__(self, host: str, ssl: bool = False):
        self.host = host
        if len(host.split('://')) == 2:
            self.host = host.split('://')[1]
        # self.username = username
        # self.password = password
        # self.key = key
        if ssl:
            self.protocol = 'https'
        else:
            self.protocol = 'http'
        self._timeout = 120
        self._header_user_agent = 'HTTPClient v.1.0'
        self._header_accept = 'application/xml, text/xml, text/plane, application/json, */*'
        self._header_accept_charset = 'utf-8'
        self._header_accept_language = 'en-US,en;q=0.8'
        self._header_content_type = 'text/plane'
        self._header_authorization = None
        self._base_url = None

    @property
    def host_url(self) -> str:
        return f'{self.protocol}://{self.host}'

    @staticmethod
    def url_with_params(url, **kwargs) -> str:
        params = '&'.join(f'{item}={if_none(kwargs[item], "")}' for item in kwargs)
        return f'{url}?{params}'

    @staticmethod
    def html_as_text(html=None):
        return html2text.html2text(str(html))

    @property
    def timeout(self):
        return self._timeout

    @timeout.setter
    def timeout(self, value: int):
        if value is not None:
            self._timeout = value

    @property
    def header_user_agent(self):
        return self._header_user_agent

    @header_user_agent.setter
    def header_user_agent(self, value: str):
        self._header_user_agent = value

    @property
    def header_content_type(self):
        return self._header_content_type

    @header_content_type.setter
    def header_content_type(self, value: str):
        self._header_content_type = value

    @property
    def header_accept(self):
        return self._header_accept

    @header_accept.setter
    def header_accept(self, value: str):
        self._header_accept = value

    @property
    def header_accept_language(self):
        return self._header_accept_language

    @header_accept_language.setter
    def header_accept_language(self, value: str):
        self._header_accept_language = value

    @property
    def header_accept_charset(self):
        return self._header_accept_charset

    @header_accept_charset.setter
    def header_accept_charset(self, value: str):
        self._header_accept_charset = value

    def headers(self) -> {}:
        headers = {
            'User-Agent': self._header_user_agent,
            'Content-Type': self._header_content_type,
            'Accept': self._header_accept,
            'Accept-Language': self._header_accept_language,
            'Accept-Charset': self._header_accept_charset,
        }
        if self._header_authorization is not None:
            headers['Authorization'] = self._header_authorization
        return headers

    @property
    def base_url(self) -> str:
        if self._base_url is None or self._base_url.strip(' ') == '':
            return f'{self.host_url}'
        return f'{self.host_url}/{self._base_url}'

    @base_url.setter
    def base_url(self, value: str):
        self._base_url = value

    def request(self, method: str, entry: str, **kwargs) -> Response:
        entry = entry.strip(' ')
        if entry is None or entry == '':
            url = f'{self.base_url}'
        else:
            url = f'{self.base_url}/{entry}'

        if 'timeout' not in kwargs:
            kwargs['timeout'] = self.timeout
        if 'headers' not in kwargs:
            kwargs['headers'] = self.headers()

        try:
            response = request(method, url, **kwargs)
        #     response.raise_for_status()
            return response
        # except HTTPError as e:
        #     logger.error(f'Service {self.host_url} request http error: {e}')
        #     return Response({'detail': f'Service {self.host_url} unavailable or get error!'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        except Exception as e:
            logger.error(f'Request {method} {url} with {kwargs} - Service {self.host_url} unavailable with {e}')
            return Response({'detail': f'Service {self.host_url} unavailable or get error!'}, status=status.HTTP_503_SERVICE_UNAVAILABLE)

    def get(self, entry, **kwargs) -> Response:
        return self.request('get', entry, **kwargs)

    def post(self, entry, **kwargs) -> Response:
        return self.request('post', entry, **kwargs)

    def put(self, entry, **kwargs) -> Response:
        return self.request('put', entry, **kwargs)

    def patch(self, entry, **kwargs) -> Response:
        return self.request('patch', entry, **kwargs)

    def delete(self, entry, **kwargs) -> Response:
        return self.request('delete', entry, **kwargs)

    def search(self, entry, **kwargs) -> Response:
        return self.request('search', entry, **kwargs)


class InternalServiceClient:

    def __init__(self, host: str, ssl: bool = False, key: str = None, username: str = None, password: str = None):
        self._client = HTTPClient(host, ssl)
        self._client.header_user_agent = f'v4_InternalServiceHTTPClient'
        self._client.header_content_type = 'application/json;charset=utf-8'
        self._client._header_authorization = f'Token {key}'
        self._username = username
        self._password = password
        self._key = key
        self._auth_entry = 'auth'

    token_updated = False

    def _update_token(self) -> bool:
        self._client._header_authorization = None
        response = self._client.post(entry=self._auth_entry, data={'username': self._username, 'password': self._password})
        if response.status_code == status.HTTP_200_OK:
            try:
                self._key = json.loads(response.content)['token']
            except:
                self.token_updated = False
                logger.error(f'Token update request response content parse error!')
                return False
            self._client._header_authorization = f'Token {self._key}'
            self.token_updated = True
            return True
        logger.error(f'Token update request return: {self._client.html_as_text(response.content)}')
        return False

    @property
    def key(self):
        return self._key

    @staticmethod
    def process_result(response):
        result = ProcessingResult()
        result.status = response.status_code
        if response.status_code in (status.HTTP_200_OK, status.HTTP_201_CREATED, status.HTTP_204_NO_CONTENT):
            try:
                result.data = json.loads(response.content)
                result.error = False
            except:
                pass
            if result.error:
                try:
                    result.data = response.data
                    result.error = False
                except:
                    logger.error(f'Get response data error: {response}')
                    result.data = {'detail': 'Get response data error!'}
                    result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        else:
            try:
                result.data = {'detail': response.data['detail']}
            except:
                result.data = {'detail': HTTPClient.html_as_text(response.text)}
        return result


class BasicAuthHTTPClient(HTTPClient):
    def __init__(self, host: str, ssl: bool = False, username: str = None, password: str = None):
        super().__init__(host, ssl)
        self._username = username
        self._password = password

    def request(self, method: str, entry: str, **kwargs) -> Response:
        kwargs['auth'] = (self._username, self._password)
        return super().request(method, entry, **kwargs)

    @property
    def username(self):
        return self._username

    @username.setter
    def username(self, value):
        self._username = value

    @property
    def password(self):
        return self._password

    @password.setter
    def password(self, value):
        self._password = value
