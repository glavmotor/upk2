from ..http_client import HTTPClient
from ..objects import ProcessingResult
import requests
from requests import request
from rest_framework import status
from rest_framework.response import Response
import logging
import html2text
import json
import ssl


logger = logging.getLogger(__name__)


class InfodecFCSClient(HTTPClient):
    def __init__(self, host: str, ssl: bool = True, ed_login: str = None, ed_pass: str = None):
        super().__init__(host, ssl)
        self._ed_login = ed_login
        self._ed_pass = ed_pass
        self.header_content_type = 'application/json'
        self._base_url = 'EDAP'
        self._session = requests.Session()

    def headers(self) -> {}:
        headers = super().headers()
        headers['ED-Login'] = self._ed_login
        headers['ED-Pass'] = self._ed_pass
        headers['timeout'] = '10000'
        return headers

    def _method_request(self, method: str = None, entry: str = None, **kwargs):
        result = ProcessingResult()
        session = requests.Session()
        # Place here adapter mounting if need
        try:
            response = session.request(method=method,
                                       url=self.url_with_params(f'{self.base_url}/{entry}', **kwargs),
                                       headers=self.headers(),
                                       timeout=self.timeout
                                       )
            if response.status_code == status.HTTP_200_OK:
                result.error = False
                result.data = response.content
            else:
                result.detail = html2text.html2text(response.text)
            result.status = response.status_code
        except Exception as e:
            result.detail = e.__str__()
            result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        return result

    def download_list(self):
        result = self._method_request('GET', 'list.php')
        if not result.error:
            data = result.data.decode('utf-8')
            result.data = []
            for item in str(data).split('\n'):
                if len(item) > 0:
                    result.data.append({'name': item.split(' ')[1], 'size': int(item.split(' ')[0])})
            return result.data
        else:
            return None

    def get_file(self, filename: str = None):
        result = self._method_request('GET', 'get.php', file=filename)
        if result.error:
            return None
        return result.data
        # if result.error:
        #     return Response({'detail': result.detail}, status=result.status)
        # return Response(result.data, status=result.status)

