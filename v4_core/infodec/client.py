from ..http_client import HTTPClient
from ..objects import ProcessingResult
from requests import request
from rest_framework import status
from rest_framework.response import Response
import logging
import html2text
import json


logger = logging.getLogger(__name__)


class InfodecClient(HTTPClient):
    def __init__(self, host: str, ssl: bool = False, key: str = None):
        super().__init__(host, ssl)
        self._key = key
        self.header_content_type = 'application/json'

    def api_request(self, function: str = None, params: dict = None, **kwargs):
        result = ProcessingResult()
        if 'timeout' not in kwargs:
            kwargs['timeout'] = self.timeout
        if 'headers' not in kwargs:
            kwargs['headers'] = self.headers()
        try:
            response = request('post',
                               f'{self.base_url}/?cmd={function}',
                               data=json.dumps({'params': params, 'api_key': self._key}),
                               **kwargs)
        except:
            logger.error(f'Service {self.host_url} unavailable!')
            result.status = status.HTTP_503_SERVICE_UNAVAILABLE
            result.data = {'detail': f'Service {self.host_url} unavailable!'}
            return result

        if response.status_code != status.HTTP_200_OK:
            logger.error(f'Service {self.host_url} returns error: {html2text.html2text(response.text)}')
            result.status = response.status_code
            result.data = {'detail': html2text.html2text(response.text)}
            return result

        data = response.json()
        if data['error_code'] in ('200', 'null'):
            result.error = False
            result.data = data['result']
            result.status = status.HTTP_200_OK
            return result
        elif response.json()['error_code'] in ('202',):
            result.error = False
            result.data = None
            result.status = status.HTTP_204_NO_CONTENT
            return result
        else:
            logger.error(f"Service {self.host_url} returns error: [{data['error_code']}] {data['description']}")
            result.data = {'detail': f"[{data['error_code']}] {data['description']}"}
            result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
            return result

    def company_logins_list(self, inn=None, kpp=None):

        params = {'Company_info': {'inn': inn,
                                   'kpp': kpp,
                                   }
                  }
        result = self.api_request('get_org_logins_list', params)
        if result.error or result.data is None:
            return result
        logins = []
        if 'Logins' in result.data:
            for key, login_raw in result.data['Logins'].items():
                # login = {'login': login_raw['login'],
                #          'login_type': login_raw['Login_type'],
                #          'declarant_id': login_raw['did'],
                #          'declarant_email': login_raw['d_email'],
                #          'tariff_id': login_raw['tariff_id'],
                #          'tariff_name': login_raw['tariff_name'],
                #          'status': login_raw['status'],
                #          'password': login_raw['pass'],
                #          }
                logins.append(login_raw)
                result.data = logins
                return result

        result.error = True
        result.data = {'detail': f'Response data parsing error! Data: {result.data}'}
        result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        return result







