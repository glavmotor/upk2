from typing import Optional, Dict
from ..models import *
from celery.utils.log import get_task_logger
import logging
from datetime import datetime, timedelta, timezone
from django.db.models import Max, Min, Q
from django.db import transaction
import dateutil.parser
import pytz
import os
from v4_core.upk2.client import UPK2Client
from django.conf import settings
import pathlib
import base64
import hashlib


# logger = logging.getLogger(__name__)
logger = get_task_logger(__name__)


log_indent = '     '


def upk2_load_packages():
    logger.info(f'Start loading...')
    config = Setting.get_section_settings('upk2')
    fbhost = FBHost.objects.get(alias=Setting.get_section_settings('common').get('current_fbhost_alias'))
    client = UPK2Client(host=config.get('api_url'),
                        ssl=config.get('api_protocol') == 'https',
                        key=config.get('api_key'))
    date_now = datetime.now(tz=timezone.utc)
    package_list = {record.shipment_gid: record.queue_datetime
                    for record
                    in Package.objects.filter(
                        queue_datetime__gte=date_now - timedelta(days=int(config.get('initial_query_depth')))
                                             )
                    }
    if len(package_list) > 0:
        datetime_from = Package.objects.filter(
                            queue_datetime__gte=date_now - timedelta(days=int(config.get('initial_query_depth'))),
        ).aggregate(Max('queue_datetime'))['queue_datetime__max'] - timedelta(days=int(config.get('query_overlapping')))
    else:
        datetime_from = date_now - timedelta(days=int(config.get('initial_query_depth')))
    logger.info(f'{log_indent}date from {datetime_from.strftime("%Y.%m.%d %H:%M:%S")}')

    result = client.packages_list(datetime_from=datetime_from)
    if result.error:
        logger.error(f"{log_indent}{result.detail}")
        return
    elif len(result.data) == 0:
        logger.info(f'{log_indent}0 packages loaded. Complete.')
        return
    data = result.data
    logger.info(f'{log_indent}{len(data)} items load')
    for item in data:
        # queue_datetime = datetime.strptime(item.get('queue_datetime'), '%Y-%m-%dT%H:%M:%SZ')
        queue_datetime = datetime.strptime(f"{item.get('StatusCreationDateTime')}+0000", '%Y.%m.%d %H:%M:%S%z')
        shipment_gid = item.get('ShipmentGID')
        if shipment_gid in package_list:
            if queue_datetime <= package_list[shipment_gid]:
                logger.info(f'{log_indent}package {shipment_gid} has been already processed')
                continue
            package = Package.objects.get(shipment_gid=shipment_gid)
            package.file_list_created = None
            package.uploaded_to_fb = None
            package.downloaded_from_upk2 = None
            package.queue_datetime = queue_datetime
            package.last_pid = package.new_pid
            package.new_pid = None
            with transaction.atomic():
                package.file_package.filter(uploaded_to_fb__isnull=True).update(uploaded_to_fb=date_now,
                                                                                upload_success=False)
                package.save()
            logger.info(f'{log_indent}package {shipment_gid} updated, processing flags cleared')
        else:
            package = Package(shipment_gid=shipment_gid,
                              queue_datetime=queue_datetime,
                              fbhost_id=fbhost.id,
                              )
            with transaction.atomic():
                package.save()
                container = Container(package_id=package.id,
                                      number=item.get('EquipmentNumber') if item.get('EquipmentNumber') is not None else 'n/a')
                container.save()
            logger.info(f'{log_indent}package {shipment_gid} with equipment {container.number} created')
    return


def upk2_create_package_file_list(package_id: int):
    package = Package.objects.get(id=package_id)
    logger.info(f'Start loading file list for package {package.shipment_gid}...')
    config = Setting.get_section_settings('upk2')
    client = UPK2Client(host=config.get('api_url'),
                        ssl=config.get('api_protocol') == 'https',
                        key=config.get('api_key')
                        )
    result = client.package_file_list(package.shipment_gid)
    if result.error:
        logger.error(f"{log_indent}{result.detail}")
        return
    elif len(result.data) == 0:
        logger.info(f'{log_indent}0 items loaded. Complete.')
        return
    data = result.data
    logger.info(f'{log_indent}{len(data)} items loaded')
    new_files = []
    for item in data:
        new_files.append(File(document_gid=item.get('DocumentGID'),
                              document_content_gid=item.get('DocumentContentGID'),
                              package_id=package.id,
                              name=item.get('DocumentAnnotation'),
                              )
                         )
    with transaction.atomic():
        File.objects.bulk_create(new_files)
        package.file_list_created = datetime.now(tz=timezone.utc)
        package.save()
    logger.info(f'{log_indent}{len(new_files)} file records added. Complete.')


def upk2_create_package_file_lists():
    for record in Package.objects.filter(file_list_created__isnull=True):
        upk2_create_package_file_list(record.id)


def upk2_load_package_files(package_id: int):
    package = Package.objects.get(id=package_id)
    logger.info(f'Start loading files for package {package.shipment_gid}...')
    package_dir_path = os.path.join(settings.PACKAGE_FILES,
                                    f'{package.shipment_gid}_{package.queue_datetime.strftime("%Y-%m-%dT%H-%M-%S")}')
    try:
        pathlib.Path(package_dir_path).mkdir(parents=True, exist_ok=True)
    except Exception as e:
        logger.error(f'{log_indent}{e}')
        return

    config = Setting.get_section_settings('upk2')
    client = UPK2Client(host=config.get('api_url'),
                        ssl=config.get('api_protocol') == 'https',
                        key=config.get('api_key')
                        )
    for record in package.file_package.filter(downloaded_from_upk2__isnull=True):
        logger.info(f'{log_indent}load file {record.name}')
        result = client.file_body(record.document_content_gid)
        if result.error:
            logger.error(f"{log_indent}{result.detail}")
            continue
        elif result.data.get('BLOB') is None and result.data.get('CBLOB') is None:
            logger.warning(f'{log_indent}data for DocumentContentGID={record.document_content_gid} has no content!')
            continue
        try:
            body = result.data.get('CBLOB') or base64.decodebytes(result.data.get('BLOB').encode('utf-8'))
        except Exception as e:
            logger.error(f'{log_indent}BLOB value decode error: {e}')
            continue
        try:
            filepath = os.path.join(package_dir_path, record.name)
            with open(filepath, 'wb') as f:
                f.write(body)
        except Exception as e:
            logger.error(f"{log_indent}save file error: {e}")
            continue
        md5 = hashlib.md5(body)
        sha256 = hashlib.sha256(body)
        # for chunk in body.chunks():
        #     md5.update(chunk)
        #     sha256.update(chunk)
        record.md5 = md5.hexdigest()
        record.sha256 = sha256.hexdigest()
        record.path = filepath
        record.mime_type = result.data.get('MimeType')
        record.downloaded_from_upk2 = datetime.now(tz=timezone.utc)
        record.save()
    if package.file_package.filter(downloaded_from_upk2__isnull=True).count() == 0:
        package.downloaded_from_upk2 = datetime.now(tz=timezone.utc)
        package.save()
        logger.info(f'{log_indent}All files loaded. Complete')


def upk2_load_packages_files():
    for record in Package.objects.filter(downloaded_from_upk2__isnull=True):
        upk2_load_package_files(record.id)


def send_status_files(package_id: int, status: str, client: UPK2Client) -> bool:
    result = True
    package = Package.objects.get(id=package_id)
    dirs = []
    for record in UPK2Doc.objects.filter(package_id=package_id, status=status, sended__isnull=True):
        logger.info(f'{log_indent}Sending file {record.name} to upk2...')
        with open(record.path, 'rb') as f:
            try:
                body = f.read()
            except Exception as e:
                logger.error(f'{log_indent}{e.__str__()}')
                result = False
                continue
            res = client.send_package_document(shipment_gid=package.shipment_gid,
                                               mime_type=record.mime_type,
                                               def_gid=record.name,
                                               content_binary=base64.encodebytes(body)
                                               )
            if res.error:
                logger.error(f'{log_indent}{res.detail}')
                result = False
                continue
            record.sended = datetime.now(tz=timezone.utc)
            record.success = True
            record.save()
            logger.info(f'{log_indent}Complete.')
            if os.path.dirname(record.path) not in dirs:
                dirs.append(os.path.dirname(record.path))
            try:
                os.remove(path=record.path)
            except Exception as e:
                logger.error(f"{log_indent}File remove error: {e}")
    for dir in dirs:
        if len(os.listdir(dir)) == 0:
            try:
                os.rmdir(dir)
            except Exception as e:
                logger.error(f"{log_indent}Directory remove error: {e}")
    return result


def upk2_send_statuses():
    logger.info(f'Start sending statuses to upk2...')
    config = Setting.get_section_settings('upk2')
    client = UPK2Client(host=config.get('api_url'),
                        ssl=config.get('api_protocol') == 'https',
                        key=config.get('api_key')
                        )
    send_count = 0
    error_count = 0
    warning_count = 0
    for record in UPK2Status.objects.filter(sended__isnull=True):
        package = record.package
        logger.info(f'{log_indent}send to {package.shipment_gid} status {record.status}...')
        files_sended = True
        if record.send_files:
            files_sended = send_status_files(package_id=package.id, status=record.status, client=client)
        if not files_sended:
            continue
        result = client.send_package_status(shipment_gid=package.shipment_gid,
                                            upk2_status=record.status,
                                            comment=record.comment)
        if result.error:
            logger.error(f"{log_indent}{result.detail}")
            error_count += 1
            continue
        record.sended = datetime.now(tz=timezone.utc)
        if int(result.data.get('ErrorCount') or 0) > 0:
            record.success = False
            record.cause = str(result.data)
            logger.warning(f'{log_indent}UPK2 return errors!')
            warning_count += 1
        else:
            record.success = True
        record.save()
        send_count += 1
    logger.info(f'{log_indent}Send {send_count} statuses with {warning_count} warning and {error_count} errors. Complete')
    return

