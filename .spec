Name:           upk2-connector
Version:        %{getenv:APP_VERSION_NUMBER}
Release:        %{getenv:APP_RELEASE}.%{getenv:APP_VERSION}
Summary:        UPK2 Connector
License:        %{getenv:DEFAULT_RPM_LICENSE}
URL:            %{getenv:DEFAULT_RPM_URL}

BuildRequires:  python36
BuildRequires:  python36-pip
BuildRequires:  python36-devel

Requires:       bash
Requires:       meta
Requires:       python36
Requires:       python36-pip
Requires:       python36-devel
Requires:       python36-virtualenv
Requires:       postgresql-devel

%description
UPK2 Connector
%{getenv:APP_FEATURES_AND_DEPENDENCIES}

%global __python %{__python3}
%define username %{getenv:DEFAULT_USERNAME}
%define group %{getenv:DEFAULT_GROUP}
%define permissions %{getenv:DEFAULT_PERMISSIONS}
%define app_name %{name}
%define app_root_folder %{getenv:DEFAULT_APP_FOLDER}
%define app_source %{_topdir}/SOURCES/%{getenv:CI_PROJECT_NAME}
%define app_folder %{app_root_folder}/%{app_name}
%define app_folder_virtualenv %{app_folder}/virtualenv
%define app_python %{app_folder_virtualenv}/bin/python
%define app_build_folder %{buildroot}%{app_folder}
%define app_pids_folder /var/spool/%{app_name}
%define systemd_folder /usr/lib/systemd/system
%define etc_folder /etc/celery

%install

mkdir -p %{app_build_folder}
mkdir -p %{app_build_folder}/pfiles
mkdir -p %{buildroot}%{systemd_folder}
mkdir -p %{buildroot}%{etc_folder}
mkdir -p %{buildroot}%{app_pids_folder}

cp -r %{app_source}/manage.py                                %{app_build_folder}/
cp -r %{app_source}/requirements.txt                         %{app_build_folder}/
cp -r %{app_source}/app                                      %{app_build_folder}/
cp -r %{app_source}/entry                                    %{app_build_folder}/
cp -r %{app_source}/v4_core                                  %{app_build_folder}/

# Make virtualenv
cd %{getenv:CI_PROJECT_DIR}
%{_bindir}/python3.6 -m pip download -r %{app_source}/requirements.txt --dest %{app_build_folder}/python_packages/

# Build
cd %{getenv:ANSIBLE_PLAYBOOK_FOLDER}
./ansible-playbook localhost:all:production -i inventory.py playbooks/build.yaml -v -e '{"build_root":"%{buildroot}", "app_name":"%{name}", "app_folder":"%{app_folder}"}'

%files
%defattr(%{permissions},%{username},%{group},%{permissions})
%{app_folder}
%{app_pids_folder}
%{systemd_folder}/*
%{etc_folder}/*

%posttrans
# make virtualenv on host
%{_bindir}/python3.6 -m virtualenv -p python3.6 %{app_folder_virtualenv}
# build and install packages from cache
%{app_python} -m pip install -r %{app_folder}/requirements.txt -f %{app_folder}/python_packages --no-index
cd %{app_folder}
export $(cat %{app_folder}/environment/%{app_name}.environment)

systemctl daemon-reload

if [ $1 -eq 1 ]; then
    systemctl enable celery.service
    systemctl enable celerybeat.service
    systemctl enable upk2-connector.service
    systemctl start celery.service
    systemctl start celerybeat.service
    systemctl start upk2-connector.service
else
    systemctl restart celery.service
    systemctl restart celerybeat.service
    systemctl restart upk2-connector.service
fi

%preun
if [ $1 -eq 0 ]; then
    systemctl stop celery.service
    systemctl stop celerybeat.service
    systemctl stop upk2-connector.service
fi
