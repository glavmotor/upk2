from .fillbill import FillBill
import logging
from rest_framework import status
from lxml import etree, objectify
from typing import Union, Optional, List, Any
from collections import defaultdict
from .objects import *
# import weakref


logger = logging.getLogger(__name__)


class Messages:

    def __init__(self, fillbill: FillBill):
        self._fillbill = fillbill
        self._messages_url = 'connectors/ru/messages'

    @property
    def fillbill(self):
        return self._fillbill

    @staticmethod
    def parse_response(response_raw: str) -> List[dict]:
        try:
            root = objectify.fromstring(response_raw)
        except etree.XMLSyntaxError:
            logger.error(f'XMLSyntaxError loading >> {response_raw}')
            return []

        items = []
        if root.countchildren() > 0:
            for message in root.message:
                data = defaultdict(dict)
                for message_property in message.getchildren():
                    data[message_property.tag] = message_property.text
                items.append(data)

        return items

    @staticmethod
    def messages_from_response(response_raw: str) -> List[Message]:
        items = Messages.parse_response(response_raw)
        messages = []
        for item in items:
            message = Message(
                id=item.get('id'),
                processed=item.get('processed'),
                createdate=item.get('createdate'),
                processeddate=item.get('processeddate'),
                preparationdatetime=f"{item.get('preparationdatetime')}+0300",
                isincoming=item.get('isincoming'),
                envelopeid=item.get('envelopeid'),
                messagecode=item.get('messagecode'),
                profileid=item.get('profileid'),
                retrycount=item.get('retrycount'),
                documentid=item.get('documentid'),
                refdocumentid=item.get('refdocumentid'),
                procid=item.get('procid', 0),
                recovery=item.get('recovery'),
                usersign=item.get('usersign'),
                initialenvelopeid=item.get('initialenvelopeid'),
                state=item.get('state'),
                processid=item.get('processid', ''),
                package_id=item.get('pid', 0),
                chain_id=item.get('chainid', 0),
                tin=item.get('tin', ''),
                exchangetype=item.get('exchangetype'),
                customscode=item.get('customscode'),
                uid=item.get('uid', 0),
                albumversion=item.get('albumversion', ''),
                usersignid=item.get('usersignid', ''),
                resendcount=item.get('resendcount'),
            )
            if item.get('initial_id') is not None:
                message.initial_message = BaseMessage(
                    id=item.get('initial_id'),
                    processed=item.get('initial_processed'),
                    createdate=item.get('initial_createdate'),
                    processeddate=item.get('initial_processeddate'),
                    preparationdatetime=item.get('initial_preparationdatetime'),
                    isincoming=item.get('initial_isincoming'),
                    envelopeid=item.get('initial_envelopeid'),
                    messagecode=item.get('initial_messagecode'),
                    profileid=item.get('initial_profileid'),
                    retrycount=item.get('initial_retrycount'),
                    documentid=item.get('initial_documentid'),
                    refdocumentid=item.get('initial_refdocumentid'),
                    procid=item.get('initial_procid')
                )

            messages.append(message)
        return messages

    def _search_request(self, **kwargs):
        if 'perpage' not in kwargs:
            kwargs['perpage'] = 100
        self.fillbill.requests_from_superuser = True
        response = self.fillbill.client.get(entry=self.fillbill.client.url_with_params(self._messages_url, **kwargs))
        self.fillbill.requests_from_superuser = False
        if response.status_code != status.HTTP_200_OK:
            logger.error(self.fillbill.client.html_as_text(response.content))
            return None
        messages = Messages.messages_from_response(response.content)
        for message in messages:
            message.fillbill = self.fillbill
        return messages

    def search(self, **kwargs):
        return self._search_request(**kwargs)

    def get_body(self, message_id: int):
        self.fillbill.requests_from_superuser = True
        response = self.fillbill.client.get(entry=f'{self._messages_url}/{message_id}')
        self.fillbill.requests_from_superuser = False
        if response.status_code != status.HTTP_200_OK:
            logger.error(self.fillbill.client.html_as_text(response.content))
            return None
        return response.content
