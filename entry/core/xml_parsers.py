from lxml import etree
import dateutil.parser
import re


def clear_namespaces(xml):
    xslt_pre = etree.XML('''\
        <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
        <xsl:output method="xml" indent="no"/>
        <xsl:template match="/|comment()|processing-instruction()">
        <xsl:copy>
            <xsl:apply-templates/>
        </xsl:copy>
        </xsl:template>
        <xsl:template match="*">
            <xsl:element name="{local-name()}">
              <xsl:apply-templates select="@*|node()"/>
            </xsl:element>
        </xsl:template>

        <xsl:template match="@*">
            <xsl:attribute name="{local-name()}">
              <xsl:value-of select="."/>
            </xsl:attribute>
        </xsl:template>
        </xsl:stylesheet>''')
    transform_pre = etree.XSLT(xslt_pre)
    return transform_pre(xml)


class XMLBaseParser:
    def __init__(self, docbody: str = None, file: str = None):
        if docbody is not None:
            if isinstance(docbody, (bytes, bytearray)):
                try:
                    self.xml = clear_namespaces(etree.fromstring(docbody))
                except:
                    self.xml = None
            else:
                try:
                    self.xml = clear_namespaces(etree.fromstring(docbody.replace('<?xml version="1.0" encoding="UTF-8"?>', '')))
                    pass
                except:
                    self.xml = None
        elif file is not None:
            try:
                self.xml = clear_namespaces(etree.parse(file))
            except:
                self.xml = None

    @property
    def initialized(self) -> bool:
        return self.xml is not None

    def get_tag_text(self, xpath: str, notexists=None):
        tags = self.xml.xpath(xpath)
        if len(tags) == 1:
            return tags[0].text
        return notexists

    def get_tag_date(self, xpath: str, notexists=None):
        text: str = self.get_tag_text(xpath, None)
        if text is None:
            return notexists
        elif text.strip(' ') == '':
            return notexists
        try:
            return dateutil.parser.parse(text)
        except:
            return notexists

    def get_tag_text_list(self, xpath: str):
        tags = self.xml.xpath(xpath)
        if tags is None:
            return []
        return [tag.text for tag in tags]


class CustomsXMLParser(XMLBaseParser):
    def __init__(self, docbody: str = None, file: str = None):
        super(CustomsXMLParser, self).__init__(docbody, file)
        self._message_parser = MessageParser(self)

    @property
    def clear_xml(self):
        return self.xml

    @property
    def message(self):
        return self._message_parser

    def build_customs_regnum(self, root_path: str = None, code_tag: str = None, date_tag: str = None, number_tag: str = None):
        splitdate = self.get_tag_text(f'//{root_path}/{date_tag}', '').split('-')
        return '/'.join((self.get_tag_text(f'//{root_path}/{code_tag}', ''),
                         ''.join((splitdate[2], splitdate[1], splitdate[0][2:])),
                         self.get_tag_text(f'//{root_path}/{number_tag}', '')))

    def regnum_code_date_number(self, root_path: str = None, code_tag: str = None, date_tag: str = None, number_tag: str = None):
        splitdate = self.get_tag_text(f'//{root_path}/{date_tag}', '').split('-')
        return '/'.join((self.get_tag_text(f'//{root_path}/{code_tag}', ''),
                         ''.join((splitdate[2], splitdate[1], splitdate[0][2:])),
                         self.get_tag_text(f'//{root_path}/{number_tag}', '')))


class MessageParser:
    def __init__(self, parser: CustomsXMLParser):
        self.parser = parser
        self._cmn13009 = CMN13009Parser(self)
        self._cmn13010 = CMN13010Parser(self)
        self._cmn13014 = CMN13014Parser(self)
        self._cmn12002 = CMN12002Parser(self)
        self._cmn12127 = CMN12127Parser(self)
        self._cmn15005 = CMN15005Parser(self)

    @property
    def type(self):
        return self.parser.get_tag_text('//MessageType') or self.parser.get_tag_text('//MessageKind')

    @property
    def preparation_datetime(self):
        return self.parser.get_tag_date('//PreparationDateTime')

    @property
    def envelope_id(self):
        return self.parser.get_tag_text('//EnvelopeID')

    @property
    def initial_envelope_id(self):
        return self.parser.get_tag_text('//InitialEnvelopeID')

    @property
    def customs_code(self):
        return self.parser.get_tag_text('//EDHeader/SenderCustoms/CustomsCode') or self.parser.get_tag_text('//EDHeader/ReceiverCustoms/CustomsCode')

    @property
    def album(self):
        soft = self.parser.get_tag_text('//SoftVersion')
        return soft.split('/')[0]


    # CMN specific parsers
    @property
    def cmn13009(self):
        return self._cmn13009

    @property
    def cmn13010(self):
        return self._cmn13010

    @property
    def cmn13014(self):
        return self._cmn13014

    @property
    def cmn12002(self):
        return self._cmn12002

    @property
    def cmn12127(self):
        return self._cmn12127

    @property
    def cmn15005(self):
        return self._cmn15005


# DO1 Report
class CMN13009Parser:
    def __init__(self, parser: MessageParser):
        self.parser = parser.parser

    @property
    def consignment_note_regnum_list(self):
        # return self.parser.get_tag_text('//GoodsShipment/TransportDocs/PrDocumentNumber')
        cns = [tag.find('PrDocumentNumber').text for tag
               in self.parser.xml.xpath('//GoodsShipment/TransportDocs')
               if tag.find('PrDocumentName').text.upper().find('НАКЛАДНАЯ') != -1
               or tag.find('PrDocumentName').text.upper().find('CMR') != -1]
        return list(set(cns))

    @property
    def do_num(self):
        return self.parser.get_tag_text('//DO1Report/ReportNumber')

    @property
    def do_date(self):
        return self.parser.get_tag_date('//DO1Report/ReportDate')


class CMN13010Parser:
    def __init__(self, parser: MessageParser):
        self.parser = parser.parser

    @property
    def do_regnum(self):
        return self.parser.build_customs_regnum(root_path='DORegInfo/RegisterNumberReport',
                                                code_tag='CustomsCode',
                                                date_tag='RegistrationDate',
                                                number_tag='GTDNumber')


class CMN13014Parser:
    def __init__(self, parser: MessageParser):
        self.parser = parser.parser

    @property
    def dt_regnum(self):
        return self.parser.get_tag_text('//WHGoodOut/ProduceDocuments/PrDocumentNumber')

    @property
    def dt_release_regnum(self):
        return self.parser.build_customs_regnum(root_path='WHGoodOut/RegisterNumber',
                                                code_tag='CustomsCode',
                                                date_tag='RegistrationDate',
                                                number_tag='GTDNumber')

    @property
    def dt_release_date(self):
        return self.parser.get_tag_date('//WHGoodOut/ReleaseDate')

    @property
    def consignment_note_regnum_list(self):
        cns = [tag.find('PrDocumentNumber').text for tag
               in self.parser.xml.xpath('//TransportDoc')
               if tag.find('PrDocumentName').text.upper().find('НАКЛАДНАЯ') != -1
               or tag.find('PrDocumentName').text.upper().find('CMR') != -1]

        for tag in self.parser.xml.xpath('//WHGoodOut/Comments'):
            patterns = re.findall('НАКЛАДНАЯ [\w\(\)\s]* от \d{2}\.\d{2}\.\d{4}', tag.text, flags=re.IGNORECASE)
            if len(patterns) > 0:
                for pattern in patterns:
                    cns.append(re.findall('\w+ от \d{2}\.\d{2}\.\d{4}', pattern)[0].split(' ')[0])

        return list(set(cns))


class CMN12002Parser:
    def __init__(self, parser: MessageParser):
        self.parser = parser.parser

    @property
    def reg_id(self):
        return self.parser.get_tag_text('//PI_DocResult/PI_RegID')

    @property
    def regnum(self):
        return self.parser.regnum_code_date_number(root_path='PI_DocResult/RegNumber',
                                                   code_tag='CountryCode',
                                                   date_tag='Date',
                                                   number_tag='PINumber')


class CMN12127Parser:
    def __init__(self, parser: MessageParser):
        self.parser = parser.parser

    @property
    def reg_id(self):
        return self.parser.get_tag_text('//PI_DocResult/PI_RegID')

    @property
    def regnum(self):
        return self.parser.regnum_code_date_number(root_path='PI_DocResult/RegNumber',
                                                   code_tag='CountryCode',
                                                   date_tag='Date',
                                                   number_tag='PINumber')


class CMN15005Parser:
    def __init__(self, parser: MessageParser):
        self.parser = parser.parser

    @property
    def regnum(self):
        return self.parser.regnum_code_date_number(root_path='NotifGTDRegistration/GTDID',
                                                   code_tag='CustomsCode',
                                                   date_tag='RegistrationDate',
                                                   number_tag='GTDNumber')


