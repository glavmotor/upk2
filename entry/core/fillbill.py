from typing import Optional, Dict
from ..models import *
from v4_core.fillbill.fillbill import FillBill
from v4_core.fillbill import objects
from celery.utils.log import get_task_logger
import logging
from datetime import datetime, timedelta, timezone
from django.db.models import Max, Min, Q
from django.db import transaction
import dateutil.parser
import pytz
from .xml_parsers import CustomsXMLParser
import os
from django.conf import settings
import pathlib
import base64


# logger = logging.getLogger(__name__)
logger = get_task_logger(__name__)


log_indent = '     '


def fb_load_contractor_events_fbhost(alias: str):
    """Loading events from the fill-bill pkg_status_log for specified status transitions"""
    fbhost = FBHost.objects.get(alias=alias)
    logger.info(f'Load contractor events from {fbhost.name}...')
    config = Setting.get_section_settings('fillbill')
    date_now = datetime.now(tz=timezone.utc)
    event_list = [record.log_id
                  for record
                  in ContractorEvent.objects.filter(
                                            log_ts__gte=date_now - timedelta(days=int(config.get('initial_query_depth'))),
                                            fbhost_id=fbhost.id
                                        )
                  ]
    chain_list = {record.chain: record.id
                  for record
                  in Package.objects.all()}
    transition_list = {f'{record.status_from or "NULL"}-{record.status_to}': record.id
                       for record
                       in ContractorStatusTransition.objects.filter(enabled=True)}
    if len(event_list) > 0:
        datetime_from = ContractorEvent.objects.filter(
                                            log_ts__gte=date_now - timedelta(days=int(config.get('initial_query_depth'))),
                                            fbhost_id=fbhost.id
                                        ).aggregate(Max('log_ts'))['log_ts__max'] - timedelta(days=int(config.get('query_overlapping')))
    else:
        datetime_from = date_now - timedelta(days=int(config.get('initial_query_depth')))

    fb = FillBill(host=fbhost.url, ssl=fbhost.ssl,
                  username=fbhost.superuser, password=fbhost.password,
                  super_username=fbhost.superuser, super_password=fbhost.password)
    result = fb.report_api.simple_report(entry='v1_pkglog_st2_ts_uid',
                                         p_uid=fbhost.connector_uid,
                                         p_status_to=','.join([str(record.status_to)
                                                               for record
                                                               in ContractorStatusTransition.objects.filter(
                                                                                enabled=True).distinct('status_to')]
                                                              ),
                                         p_ts_from=datetime_from.strftime("%Y-%m-%dT%H:%M:%SZ"),
                                         p_ts_to=date_now.strftime("%Y-%m-%dT%H:%M:%SZ"),
                                         )
    if result.error:
        logger.error(f"{log_indent}{result.data['detail']}")
        return
    elif len(result.data) == 0:
        logger.info(f'{log_indent}0 events loaded. Complete.')
        return
    new_events = []
    for item in result.data:
        if item.get('log_id') not in event_list:
            new_events.append(ContractorEvent(fbhost_id=fbhost.id,
                                              log_id=item.get('log_id'),
                                              log_ts=dateutil.parser.parse(item.get('log_ts')).astimezone(pytz.utc),
                                              pid=item.get('pid'),
                                              chain=int(item.get('chain')),
                                              status_from=item.get('status_from'),
                                              status_to=item.get('status_to'),
                                              message=item.get('message') if item.get('message') != '' else None,
                                              status_transition_id=transition_list.get(f"{item.get('status_from') or 'NULL'}-{item.get('status_to')}"),
                                              package_id=chain_list.get(int(item.get('chain'))),
                                              )
                              )
    if len(new_events) > 0:
        ContractorEvent.objects.bulk_create(new_events)
    logger.info(f'{log_indent}{len(result.data)} events loaded, {len(new_events)} events saved. Complete')
    return


def store_pair(message: objects.Message, status: str, fbhost_id: int) -> bool:
    if message.fillbill is None:
        logger.error(f"{log_indent} message {message.messagecode}(id={message.id}) doesn't have fillbill link!")
        return False
    try:
        package = Package.objects.get(fbhost_id=fbhost_id, chain=message.chain_id)
    except Package.DoesNotExist:
        logger.error(f"{log_indent}Package with chain={message.chain_id} and fbhost_id={fbhost_id} not exists!")
        return False
    body = message.body()
    initial_body = message.initial_body()
    if body is None:
        logger.error(f"{log_indent}No body received for message {message.messagecode}(id={message.id}) body")
        return False
    package_dir_path = os.path.join(settings.PACKAGE_FILES,
                                    f'{package.shipment_gid}_to_upk2')
    try:
        pathlib.Path(package_dir_path).mkdir(parents=True, exist_ok=True)
    except Exception as e:
        logger.error(f'{log_indent}{e}')
        return False
    files = {message.messagecode: body}
    file_records = []
    if initial_body is not None:
        files[message.initial_message.messagecode] = initial_body
    for item in files:
        filepath = os.path.join(package_dir_path, f'{item}.xml')
        try:
            with open(filepath, 'wb') as f:
                f.write(files[item])
        except Exception as e:
            logger.error(f"{log_indent}save file error: {e}")
            continue
        file_records.append(UPK2Doc(package_id=package.id,
                                    name=f'{item}.xml',
                                    mime_type='application/xml',
                                    path=filepath,
                                    status=status,
                                    )
                            )
    UPK2Doc.objects.bulk_create(file_records)
    return True


def envelope_id_list_from_date(date_from: datetime = None):
    return [record.envelope_id
            for record
            in Message.objects.filter(fb_create_date__gte=date_from)
            ]


def fb_load_messages_fbhost_code(alias: str, code: str):
    """loading customs messages from fill-bill connector for specified message code"""
    fbhost = FBHost.objects.get(alias=alias)
    logger.info(f'Load {code} messages from {fbhost.name}...')
    config = Setting.get_section_settings('fillbill')
    date_now = datetime.now(tz=timezone.utc)
    date_from = date_now - timedelta(days=int(config.get('initial_query_depth')))
    message_list = envelope_id_list_from_date(date_from=date_from)
    chain_list = {record.chain: record.id
                  for record
                  in Package.objects.all()}
    pair_list = {f'{record.initial_code or "NULL"}-{record.code}': {'id': record.id,
                                                                    'send_pair': record.state.upk2_send_pair_bodies,
                                                                    'status': record.state.upk2_status}
                 for record
                 in MessagesPair.objects.filter(enabled=True)}
    if len(message_list) > 0:
        datetime_from = Message.objects.filter(
                            fb_create_date__gte=date_now - timedelta(days=int(config.get('initial_query_depth'))),
                            fbhost_id=fbhost.id
                        ).aggregate(Max('create_date'))['create_date__max'] - timedelta(days=int(config.get('query_overlapping')))
    else:
        datetime_from = date_now - timedelta(days=int(config.get('initial_query_depth')))

    fb = FillBill(host=fbhost.url, ssl=fbhost.ssl,
                  username=fbhost.superuser, password=fbhost.password,
                  super_username=fbhost.superuser, super_password=fbhost.password)
    kwargs = {'messagecode': code,
              'createdate_from': datetime_from.strftime("%Y-%m-%dT%H:%M:%SZ"),
              'perpage': config['messages_per_page'],
              'page': 0,
              }
    messages = fb.messages.search(**kwargs)
    if messages is None:
        logger.error(f'{log_indent}Error messages search!')
    while messages is not None and len(messages) > 0:
        new_messages = []
        for message in messages:
            if message.envelopeid not in message_list and message.chain_id in chain_list:
                pair_key = f'{message.initial_message.messagecode if message.initial_message is not None else "NULL"}-{message.messagecode}'
                pair_id = None
                send_pair = False
                if pair_key in pair_list:
                    pair_id = pair_list[pair_key]['id']
                    send_pair = pair_list[pair_key]['send_pair']
                    pair_stored = False
                    if send_pair:
                         pair_stored = store_pair(message=message, status=pair_list[pair_key]['status'], fbhost_id=fbhost.id)
                    if not send_pair or pair_stored:
                        new_message = Message(fbhost_id=fbhost.id,
                                              fb_create_date=message.createdate,
                                              customs_create_date=message.preparationdatetime,
                                              initial_envelope_id=message.initialenvelopeid,
                                              envelope_id=message.envelopeid,
                                              code=message.messagecode,
                                              customs_process_id=message.processid,
                                              message_id=message.id,
                                              album=message.albumversion,
                                              uid=message.uid,
                                              pid=message.package_id,
                                              chain=message.chain_id,
                                              processed_by_connector=message.processed,
                                              package_id=chain_list.get(message.chain_id),
                                              messages_pair_id=pair_id
                                              )
                        if message.initial_message is not None:
                            new_message.initial_code = message.initial_message.messagecode
                        new_messages.append(new_message)
        if len(new_messages) > 0:
            Message.objects.bulk_create(new_messages)
        logger.info(f"{log_indent}Page {kwargs['page']} - {len(messages)} messages processed, {len(new_messages)} saved")
        kwargs['page'] += 1
        messages = fb.messages.search(**kwargs)
        message_list = envelope_id_list_from_date(date_from=date_from)
    logger.info(f'{log_indent}Load messages complete.')
    return


def fb_load_messages_fbhost(alias: str):
    """loading customs messages from fill-bill connector for all specified message codes"""
    for record in MessagesPair.objects.filter(enabled=True).order_by('state__process_order').distinct('state__process_order','code'):
        fb_load_messages_fbhost_code(alias, record.code)
    return


def fb_process_messages_regnums():
    """Load message body end extract customs regnum for PI and ETD registration messages"""
    logger.info('Start processing...')
    date_now = datetime.now(tz=timezone.utc)
    messages = Message.objects.filter(customs_regnum_processed__isnull=True,
                                      messages_pair_id__isnull=False).order_by('fbhost_id')
    fb: Optional[FillBill] = None
    fbhost: Optional[FBHost] = None
    regnum_cnt = 0
    for message in messages:
        if message.messages_pair.state.alias not in ('pi_reg_confirmed', 'etd_reg_confirmed'):
            message.customs_regnum_processed = date_now
            continue
        if fbhost is None or fbhost.id != message.fbhost_id:
            fbhost = message.fbhost
            fb = FillBill(host=fbhost.url, ssl=fbhost.ssl,
                          username=fbhost.superuser, password=fbhost.password,
                          super_username=fbhost.superuser, super_password=fbhost.password)
        body = fb.messages.get_body(message.message_id)
        if body is None:
            logger.error(f'{log_indent}message id={message.id} body loading error!')
            continue
        parser = CustomsXMLParser(docbody=body)
        if not parser.initialized:
            logger.error(f'{log_indent}message id={message.id} body parsing error!')
            continue
        if message.code == 'CMN.12002':
            message.customs_regnum = parser.message.cmn12002.regnum or parser.message.cmn12002.reg_id or 'n/a'
        elif message.code == 'CMN.12127':
            message.customs_regnum = parser.message.cmn12127.regnum or parser.message.cmn12127.reg_id or 'n/a'
        elif message.code == 'CMN.15005':
            message.customs_regnum = parser.message.cmn15005.regnum or 'n/a'
        else:
            message.customs_regnum = 'no_parser_for'
        regnum_cnt += 1
        message.customs_regnum_processed = date_now
    Message.objects.bulk_update(messages, fields=['customs_regnum', 'customs_regnum_processed'])
    logger.info(f'{log_indent}{len(messages)} messages processed, {regnum_cnt} regnums extracted. Complete.')
    return


def fb_create_upk2_statuses():
    """Create status data records in status send queue"""
    logger.info('Start processing...')
    events = ContractorEvent.objects.filter(upk2_status_id__isnull=True,
                                            package_id__isnull=False,
                                            status_transition_id__isnull=False,
                                            status_transition__state__upk2_send=True,
                                            ).order_by('create_date')
    for event in events:
        upk2_status = UPK2Status(package_id=event.package_id,
                                 status=event.status_transition.state.upk2_status)
        if event.status_transition.state.alias == 'cr_rejected_by_declarant':
            upk2_status.comment = event.message
        package = event.package
        package.upk2_status = event.status_transition.state.upk2_status
        package.new_pid = event.pid
        with transaction.atomic():
            upk2_status.save()
            event.upk2_status_id = upk2_status.id
            event.save()
            package.save()
    logger.info(f'{log_indent}{len(events)} events processed')
    messages = Message.objects.filter(upk2_status_id__isnull=True,
                                      package_id__isnull=False,
                                      messages_pair_id__isnull=False,
                                      messages_pair__state__upk2_send=True,
                                      ).order_by('create_date')
    for message in messages:
        status = message.messages_pair.state.upk2_status
        package = message.package
        if message.messages_pair.state.upk2_with_regnum:
            status = status.replace('@regnum@', message.customs_regnum or 'empty')
        upk2_status = UPK2Status(package_id=message.package_id,
                                 status=status,
                                 send_files=message.messages_pair.state.upk2_send_pair_bodies)
        package.upk2_status = status
        with transaction.atomic():
            upk2_status.save()
            message.upk2_status_id = upk2_status.id
            message.save()
            package.save()
    logger.info(f'{log_indent}{len(messages)} messages processed. Complete.')


def fb_upload_package(package_id: int):
    upk2_package = Package.objects.get(id=package_id)
    fbhost = FBHost.objects.get(id=upk2_package.fbhost_id)
    logger.info(f'Start processing package id={upk2_package.id}({upk2_package.shipment_gid}...')
    fb = FillBill(host=fbhost.url, ssl=fbhost.ssl,
                  username=fbhost.connector_user, password=fbhost.connector_password,
                  super_username=fbhost.superuser, super_password=fbhost.password)
    if upk2_package.chain is None:
        package = fb.packages.new(receiver=2)
        if package is None:
            logger.error(f'{log_indent}Chain not created!')
            return
        upk2_package.chain = package.id
        upk2_package.last_pid = package.id
        upk2_package.new_pid = package.id
        upk2_package.save()
    elif upk2_package.new_pid is None:
        package = fb.packages.new_from_package(from_package_id=upk2_package.chain, copy_documents=False, receiver=2)
        if package is None:
            logger.error(f'{log_indent}New package in chain not created!')
            return
        if not package.copy_documents_from(upk2_package.last_pid):
            logger.error(f'{log_indent}New package pid={package.id} could not copy documents from package pid={upk2_package.last_pid}!')
            return
        upk2_package.new_pid = package.id
        upk2_package.save()
    else:
        package = fb.packages.get(pid=upk2_package.last_pid)
        if package is None:
            logger.error(f'{log_indent}Last package in chain load error!')
            return
    date_now = datetime.now(tz=timezone.utc)
    dirs = []
    error_cnt = 0
    for file_record in upk2_package.file_package.filter(uploaded_to_fb__isnull=True):
        fb_id = package.new_file(filename=file_record.name, filepath=file_record.path)
        if fb_id is None:
            error_cnt += 1
            logger.error(f'{log_indent} file "{file_record.name}" id={file_record.id} not uploaded!')
            continue
        file_record.fb_id = fb_id
        file_record.uploaded_to_fb = date_now
        file_record.save()
        if os.path.dirname(file_record.path) not in dirs:
            dirs.append(os.path.dirname(file_record.path))
        try:
            os.remove(path=file_record.path)
        except Exception as e:
            logger.error(f"{log_indent}File remove error: {e}")
    for dir in dirs:
        if len(os.listdir(dir)) == 0:
            try:
                os.rmdir(dir)
            except Exception as e:
                logger.error(f"{log_indent}Directory remove error: {e}")
    if error_cnt == 0:
        if package.commit():
            upk2_package.uploaded_to_fb = date_now;
            upk2_package.save()
            logger.info(f'{log_indent}Package commit. Complete.')
            return
        else:
            logger.error(f'Package commit error! Processing not completed.')
            return
    logger.error(f'{error_cnt} files not uploaded! Processing not completed')
    return


def fb_upload_packages():
    for package in Package.objects.filter(downloaded_from_upk2__isnull=False, uploaded_to_fb__isnull=True):
        fb_upload_package(package.id)
    return

#
# def store_test_message_15007(package_id: int) -> str:
#     try:
#         upk2_package = Package.objects.get(id=package_id)
#     except:
#         return 'Package not found!'
#     fbhost = FBHost.objects.get(id=5)
#     fb = FillBill(host=fbhost.url, ssl=fbhost.ssl,
#                   username=fbhost.superuser, password=fbhost.password,
#                   super_username=fbhost.superuser, super_password=fbhost.password)
#     package = fb.packages.get(pid=340922, messages=True)
#     if package is None:
#         return 'Package not loaded from fbhost!'
#     message = None
#     for item in package.messages:
#         if item.messagecode == 'CMN.15007':
#             message = item
#     if message is None:
#         return 'Message 15007 not found!'
#     body = message.body()
#     initial_body = message.initial_body()
#     if body is None:
#         return 'Message 15007 does not have body!'
#     package_dir_path = os.path.join(settings.PACKAGE_FILES,
#                                     f'{upk2_package.shipment_gid}_to_upk2')
#     try:
#         pathlib.Path(package_dir_path).mkdir(parents=True, exist_ok=True)
#     except Exception as e:
#         return f'Mkdir: {e.__str__()}'
#     files = {message.messagecode: body}
#     file_records = []
#     if initial_body is not None:
#         files[message.initial_message.messagecode] = initial_body
#     for item in files:
#         filepath = os.path.join(package_dir_path, f'{item}.xml')
#         try:
#             with open(filepath, 'wb') as f:
#                 f.write(files[item])
#         except Exception as e:
#             return f"Save file error: {e}"
#
#         file_records.append(UPK2Doc(package_id=upk2_package.id,
#                                     name=f'{item}.xml',
#                                     mime_type='application/xml',
#                                     path=filepath,
#                                     status='ACCEPTED',
#                                     )
#                             )
#     UPK2Doc.objects.bulk_create(file_records)
#     new_message = Message(fbhost_id=fbhost.id,
#                           fb_create_date=message.createdate,
#                           customs_create_date=message.preparationdatetime,
#                           initial_envelope_id=message.initialenvelopeid,
#                           envelope_id=message.envelopeid,
#                           code=message.messagecode,
#                           customs_process_id=message.processid,
#                           message_id=message.id,
#                           album=message.albumversion,
#                           uid=message.uid,
#                           pid=message.package_id,
#                           chain=message.chain_id,
#                           processed_by_connector=message.processed,
#                           package_id=upk2_package.id,
#                           messages_pair_id=10,
#                           )
#     if message.initial_message is not None:
#         new_message.initial_code = message.initial_message.messagecode
#     new_message.save()
#     return 'All Ok!'





