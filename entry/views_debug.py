from rest_framework.decorators import permission_classes
from rest_framework.viewsets import ViewSet
from rest_framework import permissions, status
from rest_framework.response import Response
from datetime import datetime
from django.http import HttpResponse
from .models import Setting
from v4_core.upk2.client import UPK2Client


@permission_classes((permissions.AllowAny,))
class TestViewSet(ViewSet): 
    def list(self, request):
        config = Setting.get_section_settings('upk2')
        client = UPK2Client(host=config['api_url'], ssl=config['api_protocol'] == 'https', key=config['api_key'])
        result = client.packages_to_check(datetime_from=datetime.strptime('2020-11-01', '%Y-%m-%d'))
        if result.error:
            return Response({'detail': result.detail}, status=result.status)
        else:
            return Response(result.data, status=result.status)
        # return Response({'detail': '--'}, status=status.HTTP_200_OK)