from rest_framework import status
from ..objects import ProcessingResult
from .fillbill import FillBill
import json
import logging


logger = logging.getLogger(__name__)


class ReportAPI:
    def __init__(self, fillbill: FillBill):
        self._fillbill = fillbill
        self._report_api_url = 'reports'

    @property
    def fillbill(self):
        return self._fillbill

    def _simple_report(self, entry: str = None, **kwargs):
        kwargs['o'] = 'json'
        url = f'{self._report_api_url}/{entry}'
        result = ProcessingResult()
        try:
            response = self.fillbill.client.get(self.fillbill.client.url_with_params(url, **kwargs))
        except Exception as e:
            logger.error(e.__str__())
            result.data = {'detail': e.__str__()}
            result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
            return result

        result.status = response.status_code
        if response.status_code == status.HTTP_200_OK:
            try:
                result.data = json.loads(response.content)
                result.error = False
            except:
                logger.error(f'Get response data error for: {response}')
                result.data = {'detail': 'Get response data error!'}
                result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        else:
            try:
                result.data = {'detail': response.data['detail']}
            except:
                result.data = {'detail': self.fillbill.client.html_as_text(response.text)}
        return result

    def simple_report(self, entry: str = None, **kwargs):
        result = self._simple_report(entry, **kwargs)
        if not result.error:
            if 'data' in result.data:
                result.data = result.data['data']
            else:
                result.error = True
                result.data = {'detail': f'Unexpected data structure error {result.data}'}
                result.status = status.HTTP_500_INTERNAL_SERVER_ERROR
        return result
