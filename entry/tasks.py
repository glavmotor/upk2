from app.celery import celery_app
from datetime import datetime, timezone
import logging
from celery.signals import after_setup_logger, after_setup_task_logger
from celery.utils.log import get_task_logger
from entry.core import fillbill, upk2


logger = get_task_logger(__name__)
log_indent = '     '


@after_setup_logger.connect
def setup_loggers(logger, *args, **kwargs):
    for handler in logger.handlers:
        handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S'))
        handler.setLevel('ERROR')


@after_setup_task_logger.connect
def setup_task_logger(logger, *args, **kwargs):
    for handler in logger.handlers:
        handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s [%(funcName)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S'))
        handler.setLevel('INFO')


@celery_app.task
def upk2_load_and_process():
    upk2.upk2_load_packages()
    upk2.upk2_create_package_file_lists()
    upk2.upk2_load_packages_files()
    return 'Upk2 load and process task complete!'


@celery_app.task
def upk2_send_statuses():
    upk2.upk2_send_statuses()
    return 'Upk2 send statuses task complete!'


@celery_app.task
def fb_upload_packages():
    fillbill.fb_upload_packages()
    return 'FB upload packages task complete!'


@celery_app.task
def fb_load_and_process():
    fillbill.fb_load_contractor_events_fbhost('otvt')
    fillbill.fb_load_messages_fbhost('otvt')
    fillbill.fb_process_messages_regnums()
    fillbill.fb_create_upk2_statuses()
    return 'FB load and process task complete!'



