from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from django.utils.translation import ugettext_lazy as _
from datetime import datetime, timezone
from django.db.models import Max, Min, Count, Sum


########################################################################################################################
#                                                 Classifiers                                                          #
########################################################################################################################
########################################################################################################################
#                                                Entry models                                                        #
########################################################################################################################

class Setting(models.Model):
    section = models.CharField(max_length=128)
    name = models.CharField(max_length=128)
    value = models.CharField(max_length=1024, blank=True, null=True)

    @classmethod
    def get_section_settings(cls, section=None):
        data = {}
        if section is not None:
            for setting in cls.objects.filter(section=section):
                data[setting.name] = setting.value
        return data

    @classmethod
    def set_section_setting(cls, section=None, name=None, value=None):
        try:
            setting = cls.objects.update_or_create(section=section, name=name, defaults={'section': section, 'name': name, 'value': value})
            setting.value = value
            setting.save()
            return True
        except:
            return False

    class Meta:
        unique_together = ('section', 'name')
        verbose_name_plural = 'Settings'
        verbose_name = 'Setting'
        db_table = 'entry_settings'


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):
    username = None
    first_name = None
    last_name = None
    email = models.EmailField(_('user email'), blank=False, null=False, unique=True)
    company_id = models.IntegerField(null=True, blank=True)
    first_name = models.CharField(max_length=64, null=True, blank=True)
    middle_name = models.CharField(max_length=64, null=True, blank=True)
    last_name = models.CharField(max_length=64, null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    modify_date = models.DateTimeField(auto_now=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    class Meta:
        verbose_name_plural = 'Users'
        verbose_name = 'User'
        db_table = 'entry_users'


########################################################################################################################
#                                             Fill-Bill models                                                         #
########################################################################################################################
class FBHost(models.Model):
    enabled = models.BooleanField(default=True)
    alias = models.CharField(max_length=16, blank=False, unique=True)
    name = models.CharField(max_length=128, blank=True)
    url = models.URLField(blank=False)
    ssl = models.BooleanField(default=False)
    connector_alias = models.CharField(max_length=128, blank=False)
    superuser = models.CharField(max_length=64, blank=True)
    password = models.CharField(max_length=64, blank=True)
    connector_user = models.CharField(max_length=64, blank=True)
    connector_password = models.CharField(max_length=64, blank=True)
    connector_uid = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return f'{self.name} ({self.url})'

    class Meta:
        db_table = 'entry_fb_hosts'
        verbose_name_plural = 'FB_Hosts'
        verbose_name = 'FB_Host'


class ContractorState(models.Model):
    alias = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=64)
    process_order = models.IntegerField(null=True, blank=True)
    upk2_send = models.BooleanField(default=True)
    upk2_status = models.CharField(max_length=512, null=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'entry_contractor_states'
        verbose_name_plural = 'Contractor_states'
        verbose_name = 'Contractor_state'


class ContractorStatusTransition(models.Model):
    state = models.ForeignKey('ContractorState', related_name='statustransition_state', on_delete=models.CASCADE)
    enabled = models.BooleanField(default=True)
    alias = models.CharField(max_length=32, unique=True)
    status_from = models.IntegerField(null=True, blank=True)
    comment_from = models.CharField(max_length=1024, null=True, blank=True)
    status_to = models.IntegerField()
    comment_to = models.CharField(max_length=1024, null=True, blank=True)

    def __str__(self):
        return f'{self.comment_from or "NULL"} -> {self.comment_to}'

    class Meta:
        unique_together = ('status_from', 'status_to')
        db_table = 'entry_contractor_status_transitions'
        verbose_name_plural = 'Contractor_status_transitions'
        verbose_name = 'Contractor_status_transition'


class ContractorEvent(models.Model):
    fbhost = models.ForeignKey('FBHost', related_name='contractorevent_fbhost', on_delete=models.CASCADE)
    create_date = models.DateTimeField(auto_now_add=True)
    # FillBIll package status log fields
    log_id = models.IntegerField()
    log_ts = models.DateTimeField()
    pid = models.IntegerField()
    chain = models.IntegerField()
    status_from = models.IntegerField(null=True, blank=True)
    status_to = models.IntegerField(null=True, blank=True)
    message = models.CharField(max_length=8192, null=True, blank=True)
    # Process fb data relations:
    status_transition = models.ForeignKey('ContractorStatusTransition', related_name='contractorevent_statustransition', blank=True, null=True, on_delete=models.SET_NULL)
    package = models.ForeignKey('Package', related_name='contractorevent_package', blank=True, null=True, on_delete=models.SET_NULL)
    # Send status queue relation
    upk2_status = models.ForeignKey('UPK2Status', related_name='contractorevent_upk2status', blank=True, null=True, on_delete=models.CASCADE)

    class Meta:
        unique_together = ('fbhost_id', 'log_id')
        db_table = 'entry_contractor_events'
        verbose_name_plural = 'Contractor_events'
        verbose_name = 'Contractor_event'


class CustomsStage(models.Model):
    alias = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'entry_customs_stages'
        verbose_name_plural = 'Customs_stages'
        verbose_name = 'Customs_stage'


class CustomsState(models.Model):
    stage = models.ForeignKey('CustomsStage', related_name='customsstate_stage', on_delete=models.CASCADE)
    alias = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=64)
    process_order = models.IntegerField(null=True, blank=True)
    upk2_send = models.BooleanField(default=True)
    upk2_status = models.CharField(max_length=512, null=True, blank=True)
    upk2_with_regnum = models.BooleanField(default=False)
    upk2_send_pair_bodies = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'entry_customs_states'
        verbose_name_plural = 'Customs_states'
        verbose_name = 'Customs_state'


class MessagesPair(models.Model):
    state = models.ForeignKey('CustomsState', related_name='messagespair_state', on_delete=models.CASCADE)
    enabled = models.BooleanField(default=True)
    alias = models.CharField(max_length=32, unique=True)
    name = models.CharField(max_length=128, unique=True)
    initial_code = models.CharField(max_length=10, null=True, blank=True)
    initial_comment = models.CharField(max_length=4096, null=True, blank=True)
    code = models.CharField(max_length=10)
    comment = models.CharField(max_length=4096)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('initial_code', 'code')
        db_table = 'entry_messages_pairs'
        verbose_name_plural = 'Messages_pairs'
        verbose_name = 'Messages_pair'


class Message(models.Model):
    fbhost = models.ForeignKey('FBHost', related_name='message_fbhost', on_delete=models.CASCADE)
    fb_create_date = models.DateTimeField(blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True)
    customs_create_date = models.DateTimeField(blank=True, null=True)
    initial_envelope_id = models.CharField(max_length=64, blank=True, null=True)
    initial_code = models.CharField(max_length=10, blank=True, null=True)
    envelope_id = models.CharField(max_length=64)
    code = models.CharField(max_length=10)

    customs_process_id = models.CharField(max_length=64, blank=True, null=True)
    # Connector message info:
    message_id = models.IntegerField()
    album = models.CharField(max_length=10, blank=True, null=True)
    uid = models.IntegerField(blank=True, null=True)
    chain = models.IntegerField(blank=True, null=True)
    pid = models.IntegerField(blank=True, null=True)
    processed_by_connector = models.BooleanField(default=False)
    messages_pair = models.ForeignKey('MessagesPair', related_name='message_messagespair', blank=True, null=True, on_delete=models.SET_NULL)
    package = models.ForeignKey('Package', related_name='message_package', blank=True, null=True, on_delete=models.SET_NULL)
    # Process fb data relations:
    customs_regnum_processed = models.DateTimeField(blank=True, null=True)
    customs_regnum = models.CharField(max_length=64, blank=True, null=True)
    # Send status queue relation
    upk2_status = models.ForeignKey('UPK2Status', related_name='message_upk2status', blank=True, null=True, on_delete=models.CASCADE)

    class Meta:
        db_table = 'entry_messages'
        unique_together = ('fbhost', 'envelope_id')
        verbose_name_plural = 'Messages'
        verbose_name = 'Message'


########################################################################################################################
#                                             Common models                                                         #
########################################################################################################################
class Package(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    shipment_gid = models.CharField(max_length=32, unique=True)
    queue_datetime = models.DateTimeField()
    fbhost = models.ForeignKey('FBHost', related_name='package_fbhost', on_delete=models.CASCADE)
    chain = models.IntegerField(null=True, blank=True)
    last_pid = models.IntegerField(null=True, blank=True)
    new_pid = models.IntegerField(null=True, blank=True)
    file_list_created = models.DateTimeField(null=True, blank=True)
    downloaded_from_upk2 = models.DateTimeField(null=True, blank=True)
    uploaded_to_fb = models.DateTimeField(null=True, blank=True)
    upk2_status = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        unique_together = ('chain', 'fbhost_id')
        db_table = 'entry_packages'
        verbose_name_plural = 'Packages'
        verbose_name = 'Package'


class Container(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    package = models.ForeignKey('Package', related_name='container_package', on_delete=models.CASCADE)
    number = models.CharField(max_length=32, null=True, blank=True)

    class Meta:
        unique_together = ('package', 'number')
        db_table = 'entry_containers'
        verbose_name_plural = 'Containers'
        verbose_name = 'Container'


class File(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    document_gid = models.IntegerField()
    document_content_gid = models.IntegerField()
    package = models.ForeignKey('Package', related_name='file_package', on_delete=models.CASCADE)
    name = models.CharField(max_length=512)
    mime_type = models.CharField(max_length=512, null=True, blank=True)
    path = models.CharField(max_length=1024)
    md5 = models.CharField(max_length=32, blank=True, null=True)
    sha256 = models.CharField(max_length=64, blank=True, null=True)
    downloaded_from_upk2 = models.DateTimeField(null=True, blank=True)
    uploaded_to_fb = models.DateTimeField(null=True, blank=True)
    upload_success = models.BooleanField(null=True, blank=True)
    fb_id = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'entry_files'
        verbose_name_plural = 'Files'
        verbose_name = 'File'


########################################################################################################################
#                                                  UPK2 models                                                         #
########################################################################################################################
class UPK2Status(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    package = models.ForeignKey('Package', related_name='upk2status_package', on_delete=models.CASCADE)
    status = models.CharField(max_length=128)
    comment = models.CharField(max_length=8196, blank=True, null=True)
    send_files = models.BooleanField(default=False)
    sended = models.DateTimeField(blank=True, null=True)
    success = models.BooleanField(blank=True, null=True)
    cause = models.CharField(max_length=2048, blank=True, null=True)

    class Meta:
        db_table = 'entry_upk2_status'
        verbose_name_plural = 'UPK2_Statuses'
        verbose_name = 'UPK2_Statuse'


class UPK2Doc(models.Model):
    create_date = models.DateTimeField(auto_now_add=True)
    package = models.ForeignKey('Package', related_name='upk2doc_package', on_delete=models.CASCADE)
    name = models.CharField(max_length=512)
    mime_type = models.CharField(max_length=512, null=True, blank=True)
    path = models.CharField(max_length=1024)
    status = models.CharField(max_length=128)
    sended = models.DateTimeField(blank=True, null=True)
    success = models.BooleanField(blank=True, null=True)
    cause = models.CharField(max_length=2048, blank=True, null=True)

    class Meta:
        db_table = 'entry_upk2_docs'
        verbose_name_plural = 'UPK2_Docs'
        verbose_name = 'UPK2_Doc'




