from typing import Optional, Dict
from ..models import *
from v4_core.fillbill.fillbill import FillBill
from celery.utils.log import get_task_logger
import logging
import json
from datetime import datetime, timedelta, timezone
from django.db.models import Max, Min, Q
from django.db import transaction
import dateutil.parser
import pytz
from .xml_parsers import CustomsXMLParser
import os
from v4_core.upk2.client import UPK2Client
from django.conf import settings
import pathlib
import base64
import hashlib


# logger = logging.getLogger(__name__)
logger = get_task_logger(__name__)


log_indent = '     '


def import_test_data():
    input = '[{"ShipmentGID":"upk2-338591","EquipmentNumber":["TKRU3204070"]},{"ShipmentGID":"upk2-338593","EquipmentNumber":["TKRU3291943"]},{"ShipmentGID":"upk2-338595","EquipmentNumber":["TKRU3142678"]},{"ShipmentGID":"upk2-338592","EquipmentNumber":["TKRU3290891"]},{"ShipmentGID":"upk2-338594","EquipmentNumber":["TKRU3135494"]},{"ShipmentGID":"upk2-338777","EquipmentNumber":["TKRU0411285"]},{"ShipmentGID":"upk2-338786","EquipmentNumber":["TKRU0417982"]},{"ShipmentGID":"upk2-342620","EquipmentNumber":["TKRU0422572"]},{"ShipmentGID":"upk2-339591","EquipmentNumber":["TKRU0416409"]},{"ShipmentGID":"upk2-340100","EquipmentNumber":["TKRU3055969","TKRU3290891"]},{"ShipmentGID":"upk2-340101","EquipmentNumber":["TKRU3265060"]},{"ShipmentGID":"upk2-341240","EquipmentNumber":["TKRU0412635"]},{"ShipmentGID":"upk2-341239","EquipmentNumber":["TKRU0419990"]},{"ShipmentGID":"upk2-341757","EquipmentNumber":["TKRU0424106"]}]'
    data = json.loads(input)
    date_now = datetime.now(tz=timezone.utc)
    for item in data:
        package = Package(shipment_gid=item['ShipmentGID'],
                          chain=int(item['ShipmentGID'].split('upk2-')[1]),
                          queue_datetime=date_now,
                          fbhost_id=5,)
        package.save()
        for citem in item['EquipmentNumber']:
            container = Container(package_id=package.id, number=citem)
            container.save()





