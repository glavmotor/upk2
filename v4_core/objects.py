import attr
from typing import Any, Optional


@attr.s
class ProcessingResult(object):
    data: Any = attr.ib(default=None)
    status: Optional[int] = attr.ib(default=None)
    remote_error: Optional[str] = attr.ib(default=None)
    error: bool = attr.ib(converter=bool, default=True)
    detail: str = attr.ib(default='Unknown error')
